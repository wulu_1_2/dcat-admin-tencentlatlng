<?php

namespace Hkw\TencentLatlng;

use Dcat\Admin\Extend\ServiceProvider;
use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Hkw\TencentLatlng\Form\TencentLatlng;

class TencentLatlngServiceProvider extends ServiceProvider
{

	public function register()
	{
		//
	}

	public function init()
	{
		parent::init();
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'hkw-form-tencentlatlng');
        Admin::booting(function () {
            Form::extend('tencentlatlng', TencentLatlng::class);
        });
	}
}

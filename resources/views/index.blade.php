<div class="{{$viewClass['form-group']}}">

    <label class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}}">
        @include('admin::form.error')
        <div class="row">
            <div class="col-md-3">
                <input id="{{$name['lat']}}" name="{{$name['lat']}}" class="form-control" value="{{ old($column['lat'], $value['lat'] ?? null) }}"/>
            </div>
            <div class="col-md-3">
                <input id="{{$name['lng']}}" name="{{$name['lng']}}" class="form-control" value="{{ old($column['lng'], $value['lng'] ?? null) }}"/>
            </div>
            <div class="col-md-6 col-md-offset-6">
                <div class="input-group">
                    <input type="text" class="form-control" id="search-{{$name['lat'].$name['lng']}}" placeholder="请输入完整的地址，含省市区">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" id="searchbtn-{{$name['lat'].$name['lng']}}"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
        </div>
        <br>
        <div id="map_{{$name['lat'].$name['lng']}}" style="width: 100%;height: {{$height}}px"></div>
        @include('admin::form.help-block')
    </div>
</div>
